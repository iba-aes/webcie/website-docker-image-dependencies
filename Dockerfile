FROM php:8.1-cli

SHELL ["/bin/bash", "-c"]

RUN apt-get update

# Install dependencies
RUN apt-get install -y software-properties-common gpg python3 python3-pip vim sudo subversion git zip \
  unzip gettext libxml2 libxml2-dev libc-client-dev libkrb5-dev libldap2-dev libmcrypt-dev libpng-dev \
  libyaml-dev default-mysql-client locales libzip-dev libonig-dev python3-dev default-libmysqlclient-dev \
  build-essential

# Make sure php.ini is correct
RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
RUN sudo pecl config-set php_ini /usr/local/etc/php/php.ini
RUN sudo pear config-set php_ini /usr/local/etc/php/php.ini
RUN echo -e "short_open_tag = On\n" >> /usr/local/etc/php/php.ini

# Install PHP extensions
RUN pecl install apcu-5.1.23
RUN docker-php-ext-enable apcu \
  && docker-php-ext-install zip \
  && docker-php-ext-install dom \
  && docker-php-ext-configure imap --with-kerberos --with-imap-ssl && docker-php-ext-install imap \
  && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && docker-php-ext-install ldap \
  && docker-php-ext-install mbstring \
  && docker-php-ext-install pdo pdo_mysql \
  && docker-php-ext-install gd \
  && docker-php-ext-install gettext

#TODO: Vind hier iets voor
#RUN docker-php-ext-install mcrypt

# Install YAML extension
RUN sudo pecl channel-update pecl.php.net
RUN sudo pecl install yaml && docker-php-ext-enable yaml
RUN sudo pecl install xdebug

# Generate locales
RUN sudo echo -e "nl_NL ISO-8859-1\n" >> /etc/locale.gen
RUN sudo echo -e "nl_NL.UTF-8 UTF-8\n" >> /etc/locale.gen
RUN sudo echo -e "nl_NL@euro ISO-8859-15\n" >> /etc/locale.gen
RUN sudo /usr/sbin/locale-gen

# Install npm and bower
RUN apt-get install -y openjdk-17-jdk
RUN apt-get install -y nodejs npm
RUN npm install bower -g

RUN ln -s /usr/local/bin/php /usr/bin/php

# Install python dependencies
RUN apt-get install -y build-essential libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev \
  libffi-dev zlib1g-dev libsqlite3-dev

# Make new non-root user
RUN useradd -ms /bin/bash dockeruser
RUN echo "dockeruser ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN chown -R dockeruser:dockeruser /var/www/html

# Make /srv/content
RUN mkdir /srv/content
RUN chown dockeruser:dockeruser /srv/content

USER dockeruser

# Install pyenv
# Om Pyenv te gebruiken moet eerst "RUN eval $PY_INIT; <python code>" gedaan worden
ENV PYENV_ROOT /home/dockeruser/.pyenv
ENV PYENV_BIN $PYENV_ROOT/bin
ENV PATH $PYENV_BIN:$PATH
ENV PY_INIT="eval \"\$(pyenv init -)\"; eval \"\$(pyenv virtualenv-init -)\";"
RUN curl https://pyenv.run | bash
RUN echo "export PATH=\"$PYENV_BIN:\$PATH\"; eval \"\$(pyenv init -)\"; eval \"\$(pyenv virtualenv-init -)\"" >> ~/.bashrc
RUN pyenv install 3.12

USER root
